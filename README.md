# **MAPAS CONCEPTUALES: CONSULTARÍA DE SOFTWARE** :office: :computer:
## *La consultoría*

```plantuml
@startmindmap
caption **18161151: Lazaro Lopez Juan Luis**
title **La consultoría**

*[#0CACED] **La consultaría**
 *_ Tiene el \npapel de
  *[#93D4FE] Prestación de servicios \nde alto valor añadido
   *_ Sus caracteristicas son
    *[#93D4FE] Necesitan tener un \nconocimiento sectorial \nimportante
    *[#93D4FE] Se requiere un conocimiento \ntecnico importante (a la vanguardia)
    *[#93D4FE] Capacidad constante de \nplateamiento de soluciones
     *_ A
      *[#93D4FE] problemas del mundo \nempresarial frecuentes 
 *_ Nace por 
  *[#A077BB] Las grandes organizaciones, o \nclientes de las consultoras
   *_ Que
    *[#A077BB] Internamente tienen \nmucha complejidad
     *_ En sus
      *[#A077BB] Procesos internos
       *_ Y requieren
        *[#A077BB] Sistemas de información \nque den soporte a \nesos procesos complejos
 *_ Las
  *[#F09545] Tipologias de servicio
   *_ Son
    *[#E58DCC] Consultoría
     *_ Tiene \nactividades \ncomo
      *[#E58DCC] Reingeniería de procesos
       *_ Es decir
        *[#E58DCC] Ayudar a la empresas a \ncomo organizar sus actividades
         *_ A traves \nde
          *[#E58DCC] Determinados esquemas \nde funcionamiento
           *_ A medida que
            *[#E58DCC] Sea mas optimo \nque el actual
      *[#E58DCC] Gobierno TI
       *_ Tiene que \nver con
        *[#E58DCC] Calidad
        *[#E58DCC] Metodologias de desarrollo
        *[#E58DCC] Arquitectura empresarial
      *[#E58DCC] Oficina de proyecto
      *[#E58DCC] Analitica avanzada de datos
       *_ Ya que
        *[#E58DCC] Es importante disponer de \ninformación en tiempo real 
         *_ Para
          *[#E58DCC] La toma de desiciones
    *[#97CB72] Integración
     *_ Tiene que \nver con
      *[#97CB72] El mapa de sistemas
       *_ Con el \nque
        *[#97CB72] Da soporte a su \nactividad de negocio
         *_ El cual \ninvolucra
          *[#97CB72] Piezas \nSoftware
           *_ Como
            *[#97CB72] Piezas \nHardware
     *_ Tiene \nactividades \ncomo
      *[#97CB72] Desarrollos \na medida
       *_ En o con
        *[#97CB72] Diferentes tipos de\nlenguajes y tecnologias
         *_ Como
          *[#97CB72] Tecnologias abiertas  
      *[#97CB72] Aseguramiento \nde la calidad E2E
       *_ Conlleva
        *[#97CB72] Asegurar la \ncalidad de software
         *_ Y
          *[#97CB72] Solución de problemas \nde sistemas  
      *[#97CB72] Infraestructuras
       *_ Como
        *[#97CB72] El uso de \nnuevas tecnologias
         *_ Como
          *[#97CB72] Plataformas
      *[#97CB72] Soluciones \nde mercado
       *_ Para
        *[#97CB72] Entender la problematica \no demanda del cliente
         *_ Y
          *[#97CB72] Dar soluciones viables
    *[#C7CAEE] Externalización
     *_ Trata de
      *[#C7CAEE] Definir organizaciones de \nservicios gobernadas a largo plazo
       *_ En las \nque
        *[#C7CAEE] Se asume actividades que \nno son core del negocio
         *_ De
          *[#C7CAEE] Sus clientes
           *_ Para
            *[#C7CAEE] Ejecutarlas \nen su nombre
             *_ de manera
              *[#C7CAEE] Mas eficiente \n y eficacia
     *_ Tiene \nactividades \ncomo
      *[#C7CAEE] Gestión de \naplicaciones
       *_ Se da cuando
        *[#C7CAEE] Las empresas delegan el \nmantenimiento y evolución
         *_ De
          *[#C7CAEE] Sus sistemas \nen un tercero
           *_ Regulado \npor
            *[#C7CAEE] Un acuerdo a \nnivel de servicio
      *[#C7CAEE] Servicios \nSQA
       *_ Conlleva
        *[#C7CAEE] El sourcing del testing \nde aplicaciones
      *[#C7CAEE] Operación y adm. de \ninfraestructura
       *_ Tiene que \nver con
        *[#C7CAEE] Sourcing y BPOs
      *[#C7CAEE] Procesos de negocio
 *_ Es una
  *[#D67AF6] Profesion \na futuro 
   *_ Ya que \npermite
    *[#83A0BE] Alcanzar la \nfelicidad profesional
     *_ Ya que \nofrece
      *[#83A0BE] Retribución competitiva
      *[#83A0BE] Carrera profesional
      *[#83A0BE] Retos
   *_ Ya que
    *[#79DE9D] Tiene un papel clave como \nproveedor de servicios
     *_ De 
      *[#79DE9D] Habilitación \ndigital
       *_ Los cuales \ntienen
        *[#79DE9D] Un papel clave en las \ngrandes iniciativas a futuro
         *_ En
          *[#79DE9D] Transformación digital
          *[#79DE9D] Industria conectada 4.0
          *[#79DE9D] Smart Cities
 *_ Su
  *[#DE79A2] Exigencia
   *_ Ser
    *[#C1E2CD] Buenos profesionales
     *_ Por su
      *[#C1E2CD] Formación y experiencia
      *[#C1E2CD] Capacidad de trabajo y evolución
     *_ Capaces de
      *[#C1E2CD] Colaborar en un proyecto en común
      *[#C1E2CD] Trabajar en equipo 
      *[#C1E2CD] Ofrecer honestidad y sinceridad
   *_ Como
    *[#D2C1E2] Enfrentar retos de manera constante
   *_ En
    *[#9B2FD5] Actitud
     *_ Se requiere   
      *[#9B2FD5] Proactividad
       *_ Implica
        *[#9B2FD5] Iniciativa y anticipación
       *_ Es imprescindible
        *[#9B2FD5] En consultería 
      *[#9B2FD5] Voluntad de mejora
       *_ Requiere
        *[#9B2FD5] Actitud activa
         *_ En
          *[#9B2FD5] Construcción de empresas
        *[#9B2FD5] Aportación de ideas de mejora  
      *[#9B2FD5] Responsabilidad
       *_ Para
        *[#9B2FD5] Asumir los trabajos para hacerlos
        *[#9B2FD5] No devolver los nuevos problemas
         *_ Y
          *[#9B2FD5] Buscar soluciones con autonomia
        *[#9B2FD5] No esgrimar razones para \nno llevar acabo un trabajo
   *_ Requiere   
    *[#2F9BD5] Disponibilidad total    
    *[#2F9BD5] Asumir nuevos retos
     *_ Ya \nque
      *[#2F9BD5] Actuar por encima de \nconocimientos propios
      *[#2F9BD5] Permiten evolucionar \nmás rápido
      *[#2F9BD5] Puede ser estresante
    *[#2F9BD5] Viajes
     *_ Ya \nque
      *[#2F9BD5] Suele demandar movilidad geográfica
 *_ Como es
  *[#D5642F] La carrera \nprofesional
   *_ Suelen tener 
    *[#72C1F4] Un modelo de carrera
     *_ Que incluye
      *[#72C1F4] Modelo estandar de categorias
      *[#72C1F4] Plan de carrera
      *[#72C1F4] Proceso de evaluación
      *[#72C1F4] Plan de formación
     *_ Que es
      *[#72C1F4] Flexible
      *[#72C1F4] Ajustado a las necesidades \ndel mercado
      *[#72C1F4] Calido para diferentes \ntipos de servicios
   *_ Ejemplo de
    *[#72F4AB] Categorías profesionales
     *_ En el
      *[#72F4AB] Ambito de consultoría
       *_ Se divide \nen
        *[#72F4AB] Junior
         *_ Se divide
          *[#72F4AB] Asistente
          *[#72F4AB] Consultor Junior
          *[#72F4AB] Consultor
        *[#72F4AB] Senior
         *_ Se divide
          *[#72F4AB] Consultor Senior
          *[#72F4AB] Jefe de equipo
          *[#72F4AB] Jefe de proyecto
        *[#72F4AB] Gerente
        *[#72F4AB] Director
@endmindmap
```

## *Consultoría de Software*

```plantuml
@startmindmap
caption **18161151: Lazaro Lopez Juan Luis**
title **Consultoría de Software**

*[#0CACED] **Consultoría de Software**
 *_ Hace
  *[#A572CB] Desarrollo de software
   *_ Es
    *[#A572CB] Un proceso muy largo
 *_ Se \ndesarrolla
  *[#0AC9D9] Comienza con la necesidad o solicitud del cliente 
   *_ Como ejemplo
    *[#0AC9D9] El desarrollo de una aplicacion para venta de un producto
  *[#0EF907] Consultería
   *_ en el cual
    *[#0EF907] Se ven los requerimientos \ndel software
    *[#0EF907] Se hace un estudio \nde viabilidad
     *_ Donde se \nanaliza
      *[#0EF907] Los necesidades o requeimientos
      *[#0EF907] Las ventajas
      *[#0EF907] El Coste que conlleva
      *[#0EF907] La viabilidad
  *[#4971F7] Diseño funcional
   *_ Consiste \nen
    *[#4971F7] La informacion necesaria \npara el software
     *_ En
      *[#4971F7] El sistema
       *_ De
        *[#4971F7] Entrada
        *[#4971F7] Salida
     *_ Se almacena \nen
      *[#4971F7] Una base de datos
   *_ Se ve
    *[#4971F7] El modelo de datos   
    *[#4971F7] Numero de procesos 
     *_ De 
      *[#4971F7] Entrada
      *[#4971F7] Salida
      *[#4971F7] Repetitivos
    *[#4971F7] El prototipo  
   *_ Es necesario
    *[#4971F7] Entender el \n"lenguaje" del cliente  
  *[#DD81F3] Diseño técnico
   *_ Donde
    *[#DD81F3] Se aisla el tipo de servicio de negocio
     *_ De
      *[#DD81F3] La parte tecnica
   *_ Se traslada
    *[#DD81F3] El diseño a nivel tecnico
     *_ Con
      *[#DD81F3] Algun lenguaje de programación
      *[#DD81F3] Base de datos
   *_ Y también
    *[#DD81F3] Se realizan pruebas 
     *_ Llamada
      *[#DD81F3] Prueba integrada
  *[#6092F0] Pruebas funcionales
   *_ Para
    *[#6092F0] Verficar que cumpla \nlos requerimientos
     *_ Del
      *[#6092F0] Cliente 
  *[#FEFAA7] Pruebas de usuario
   *_ Por
    *[#FEFAA7] El cliente
  *[#71FF8B] Se implanta en donde \nlo requiera el cliente  
 *_ Se
  *[#71FFD0] Desarrollan actividades
   *_ Que
    *[#71FFD0] Permiten crear o mantener
     *_ La
      *[#71FFD0] Infromatica de una empresa
       *_ La cual
        *[#71D0FF] Para funcionar y mantenerse
         *_ Requiere
          *[#71D0FF] Infraestructura
           *_ Como
            *[#71D0FF] Ordenadores
            *[#71D0FF] Servidores
             *_ Donde
              *[#71D0FF] Se almacena la información
            *[#71D0FF] Habilitar la infraestructura de comunicaciónes
            *[#71D0FF] Seguridad
          *[#71D0FF] Software
           *_ Que permitan
            *[#71D0FF] Desarrollar su negocio
        *[#FF71E5] Para seguir o volver a funcionar
         *_ Depende
          *[#FF71E5] El volumen de la empresa
         *_ Requiere
          *[#FF71E5] Una serie de mecanismos
          *[#FF71E5] Equipo de personas monitoreando
           *_ que
            *[#FF71E5] Procesos se estan lanzando
          *[#FF71E5] La infraestructura y comunicaciones funciones
          *[#FF71E5] La informacion sea accesible
          *[#FF71E5] Las aplicaciones se adecuen a los cambios
           *_ Y
            *[#FF71E5] Que no falle
       *_ Tiene
        *[#F1F119] El factor escala
         *_ Se 
          *[#F1F119] Se adecua dependiendo
           *_ De la \nnecesidad de
            *[#F1F119] Servicio 
            *[#F1F119] Proyecto
            *[#F1F119] Cliente
         *_ Se tiene que
          *[#F1F119] Conocer el sector del cliente
         *_ Donde
          *[#F1F119] Tener una estructura personalizada
          *[#F1F119] Servicios generalistas
       *_ Donde hay
        *[#19F19F] Sectores con funcionamiento critico
         *_ La cual
          *[#19F19F] Depende del negocio
          *[#19F19F] Todos los sectores tiene procesos criticos
 *_ Sus
  *[#A07DC4] Tendencias actuales
   *_ Han cambiado
    *[#74FF6F] El concepto de infraestructura de una empresa
     *_ Antes 
      *[#74FF6F] Cada empresa tenia sus propios ordenadores
     *_ Ahora
      *[#74FF6F] Funciona  traves de la nube
       *_ Es decir
        *[#74FF6F] Tener información en servidores
         *_ Y
          *[#74FF6F] La seguridad la realiza \nlas empresas especializada
           *_ Como ejemplo
            *[#74FF6F] Google
      *[#74FF6F] A nivel de metodologia 
       *_ Donde
        *[#74FF6F] Se han desarrollado metodologias agiles e interactivas
         *_ Que
          *[#74FF6F] Permiten el mejor desarrollo       
   *_ Sus
    *[#26EBF1] Desventajas
     *_ Son
      *[#26EBF1] Inconvenientes de clientes \ncon respecto tercers 
 *_ Un ejemplo es
  *[#F126CF] AXPE Consulting
   *_ Se dedica
    *[#26F1F1] Implantar sistemas informaticos \nen grandes empresas
     *_ Y
      *[#26F1F1] Desarrollar software para estas     
   *_ Esta
    *[#F0ADE8] Es una organización de Madrid, España
    *[#F0ADE8] Presta servicio a 4 paises directamente
    *[#F0ADE8] Presta servicio a 20 paises indirectamente
     *_ En
      *[#F0ADE8] Europa
      *[#F0ADE8] Latinoamerica
   *_ Prestan
    *[#C6F31F] Servicios de consultoría
   *_ Utlizan 
    *[#1FF39C] Tecnicas y métodos avanzado
     *_ Para
      *[#1FF39C] Prestar servicios de alto servicio
       *_ Tanto
        *[#1FF39C] En la capacidad de servicio
        *[#1FF39C] Uso de las tecnologias
@endmindmap
```

## *Aplicación de la ingeniería de software*

```plantuml
@startmindmap
caption **18161151: Lazaro Lopez Juan Luis**
title **Aplicación de la ingeniería de software**

*[#0CACED] **Aplicación de la ingeniería de software**
 *_ Un ejemplo es 
  *[#D88AF0] Stratesys
   *_ Es
    *[#D88AF0] Empresa de servicios de tecnologia
     *_ Que
      *[#D88AF0] Lleva en el mercado 15 años
     *_ Tiene un enfoque
      *[#D88AF0] A la metodologia   
 *_ El
  *[#FF5F5F] Enfoque metodologico
   *_ entorno
    *[#FF5F5F] A desarrollar una aplicación
     *_ Conlleva
      *[#FFB5E9] Una metologia
       *_ Tanto 
        *[#B4EEDA] Internamente 
        *[#B4EEDA] Externamente
       *_ Es
        *[#CFCBF2] Estandar
         *_ Conlleva
          *[#CFCBF2] Recoger los requisitos
           *_ Es decir
            *[#CFCBF2] Lo que necesita el cliente
            *[#CFCBF2] El area donde se implementara
          *[#CFCBF2] Reunirse con las áreas que interviene
           *_ Como
            *[#CFCBF2] Área de sistemas
           *_ Ademas de
            *[#CFCBF2] Responsables
             *_ Como
              *[#CFCBF2] Director financiero
              *[#CFCBF2] Director de compra
              *[#CFCBF2] Director de Venta
           *_ Para
            *[#CFCBF2] Tener una vision global de la empresa
             *_ Y
              *[#CFCBF2] Enteder los procesos de la empresa
          *[#CFCBF2] Se genera un documento
           *_ El cual es
            *[#CFCBF2] Requisito funcional
             *_ En el cual
              *[#CFCBF2] Ellos detallan los requisitos
            *[#CFCBF2] Analisis funcional  
             *_ En el cual
              *[#CFCBF2] Se detalla como se hara
          *[#CFCBF2] Plan de proyecto
           *_ En cual se detalla
            *[#CFCBF2] Que tareas se realizaran
            *[#CFCBF2] Quienes realizaran las tareas
            *[#CFCBF2] El espacio temporal
       *_ Donde también
        *[#40FE60] Se realiza un analisis tecnico  
        *[#EAFD62] Diseño tecnico 
         *_ Tanto a
          *[#EAFD62] Nivel de programación
          *[#EAFD62] Nivel de parametrización
     *_ Se divide
      *[#F986D8] En tres fases
       *_ Que son
        *[#B6F4FF] Analisis
         *_ En el cual 
          *[#B6F4FF] Define y como se hara el proyecto
        *[#EEB6FF] Construcción
         *_ Se divide en
          *[#EEB6FF] Prototipado
           *_ Donde 
            *[#EEB6FF] Primera version de lo que se tendra
          *[#EEB6FF] Gestión de alcance
           *_ En el cual
            *[#EEB6FF] Se verifica (Con el cliente)
          *[#EEB6FF] Cambios de alcance
           *_ En el cual
            *[#EEB6FF] Se verifica que cambios ha habido
        *[#6EEFB1] Pruebas
         *_ Donde
          *[#6EEFB1] Se tiene que se autonomo en las primeras pruebas
          *[#6EEFB1] Las siguientes se hacen grupo
          *[#6EEFB1] Se ceden al usuario
         *_ Estas
          *[#6EEFB1] Se deben de documentar 
@endmindmap
```
## Recursos :mag_right:
### La consultoría
[La consultoría. Una profesión de éxito presente y futuro](https://canal.uned.es/video/5a6f887cb1111f81638b4570)  

### Consultoría de Software
[Desarrollo de Software](https://canal.uned.es/video/5a6f1733b1111f35718b4580)  
[Actividades desarrolladas en AXPE Consulting](https://canal.uned.es/video/5a6f1734b1111f35718b4586)  
[Tendencias en el desarrollo de servicios](https://canal.uned.es/video/5a6f1730b1111f35718b456a)  
[Visita técnica a una consultora de software: AXPE Consulting](https://canal.uned.es/video/5a6f1734b1111f35718b458b)  

### Aplicación de la ingeniería de software
[Stratesys: Aplicación de la ingeniería de software](https://canal.uned.es/video/5a6f4c4eb1111f082a8b4978)  